package net.openesb.plugin.assemblyviewer.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import net.openesb.plugin.assemblyviewer.model.PluginInfo;
import net.openesb.plugin.assemblyviewer.module.MapModule;
import net.openesb.plugin.assemblyviewer.spi.AssemblyViewerService;
import net.openesb.standalone.Lifecycle;
import net.openesb.standalone.plugins.Plugin;

import com.google.inject.Module;

/**
 * Statistic collector service.
 * 
 * @author ldassonville
 *
 */
public class AssemblyViewerPlugin implements Plugin {
	
	private static PluginInfo pluginInfo;

	/**
	 * Load plugin informations
	 */
	private static void loadInfos(){
		
		ResourceBundle bundle = ResourceBundle.getBundle("plugin");
		pluginInfo = new PluginInfo();
		pluginInfo.setDescription(bundle.getString("description"));
		pluginInfo.setName(bundle.getString("name"));
		pluginInfo.setVersion(bundle.getString("version"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String name() {
		return pluginInfo.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	public String description() {
		return  pluginInfo.getDescription();
	}
	
	
	/**
	 * Return plugin informations
	 * @return
	 */
	public static PluginInfo getInfos() {
		if(pluginInfo == null){
			loadInfos();
		}
		return pluginInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<Class<? extends Module>> modules() {
		List<Class<? extends Module>> modules = new ArrayList<Class<? extends Module>>();
		modules.add(MapModule.class);
		return modules;
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<Class<? extends Lifecycle>> services() {
		List<Class<? extends Lifecycle>> services = new ArrayList<Class<? extends Lifecycle>>();
		services.add(AssemblyViewerService.class);
		return services;
	}
	    
}