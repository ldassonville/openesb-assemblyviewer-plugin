package net.openesb.plugin.assemblyviewer.utils;

import com.sun.jbi.management.descriptor.Consumer;
import com.sun.jbi.management.descriptor.Consumes;
import com.sun.jbi.management.descriptor.Provider;
import com.sun.jbi.management.descriptor.Provides;

public final class UIDUtils {

    private static final String UID_PATTERN = "%s_%s_%s";

    private UIDUtils() {    
    }
    
    public static String getUID(Consumes consumes) {
        return String.format(UID_PATTERN, consumes.getServiceName().getLocalPart(), consumes.getEndpointName(), consumes.getInterfaceName().getLocalPart());
    }

    public static String getUID(Provides provides) {
        return String.format(UID_PATTERN, provides.getServiceName().getLocalPart(), provides.getEndpointName(), provides.getInterfaceName().getLocalPart());
    }

    public static String getUID(Consumer consumes) {
        return String.format(UID_PATTERN, consumes.getServiceName().getLocalPart(), consumes.getEndpointName(), consumes.getInterfaceName().getLocalPart());
    }

    public static String getUID(Provider provides) {
        return String.format(UID_PATTERN, provides.getServiceName().getLocalPart(), provides.getEndpointName(), null);
    }
}
