package net.openesb.plugin.assemblyviewer.utils;

import com.sun.jbi.management.descriptor.Consumes;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Provides;
import com.sun.jbi.management.descriptor.Services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.openesb.plugin.assemblyviewer.model.map.Assembly;
import net.openesb.plugin.assemblyviewer.model.map.Endpoint;
import net.openesb.plugin.assemblyviewer.model.map.Process;

public final class Mapper {

    private Mapper() {
        
    }

    public static Assembly getCasa(String name, Jbi jbi) {

        net.openesb.plugin.assemblyviewer.model.map.Assembly casa = new Assembly();
        casa.setName(name);
        casa.setProcesses(new ArrayList<Process>());

        Map<String, List<Consumes>> mapConsumes = new HashMap<String, List<Consumes>>();
        Map<String, List<Provides>> mapProvides = new HashMap<String, List<Provides>>();

        Services services = jbi.getServices();

        //Extract process consumes
        List<Consumes> consumes = services.getConsumes();
        for (Consumes consume : consumes) {
            net.openesb.plugin.assemblyviewer.model.map.Consumes wrapper = new net.openesb.plugin.assemblyviewer.model.map.Consumes(consume);
            List<Consumes> processConsumes = mapConsumes.get(wrapper.getProcessName());
            if (processConsumes == null) {
                processConsumes = new ArrayList<Consumes>();
            }
            processConsumes.add(consume);
            mapConsumes.put(wrapper.getProcessName(), processConsumes);
        }

        //Extract process provide
        List<Provides> provides = services.getProvides();
        for (Provides provide : provides) {
            net.openesb.plugin.assemblyviewer.model.map.Provides wrapper = new net.openesb.plugin.assemblyviewer.model.map.Provides(provide);
            List<Provides> processProvides = mapProvides.get(wrapper.getProcessName());
            if (processProvides == null) {
                processProvides = new ArrayList<Provides>();
            }
            processProvides.add(provide);
            mapProvides.put(wrapper.getProcessName(), processProvides);
        }


        Set<String> processNames = new HashSet<String>();
        processNames.addAll(mapProvides.keySet());
        processNames.addAll(mapConsumes.keySet());

        for (String processName : processNames) {
            Process process = new Process();
            casa.getProcesses().add(process);
            process.setName(processName);

            List<Consumes> processConsumes = mapConsumes.get(processName);
            process.setOutputs(new ArrayList<Endpoint>());

            if (processConsumes != null) {
                for (Consumes processConsume : processConsumes) {
                   net.openesb.plugin.assemblyviewer.model.map.Consumes wrapper = 
                            new net.openesb.plugin.assemblyviewer.model.map.Consumes(processConsume);
                    
                    Endpoint endpoint = new Endpoint();
                    endpoint.setName(wrapper.getDisplayName());
                    process.getOutputs().add(endpoint);

                    //Connector identifier elements
                    endpoint.setConnectionPoint(ConnectionPointBuilder.getConnectionPoint(processConsume));

                }
            }

            List<Provides> processProvides = mapProvides.get(processName);
            process.setInputs(new ArrayList<Endpoint>());

            if (processProvides != null) {
                for (Provides processProvide : processProvides) {
                    net.openesb.plugin.assemblyviewer.model.map.Provides wrapper = 
                            new net.openesb.plugin.assemblyviewer.model.map.Provides(processProvide);
                    
                    Endpoint endpoint = new Endpoint();
                    endpoint.setName(wrapper.getDisplayName());
                    process.getInputs().add(endpoint);

                    //Connector identifier elements
                    endpoint.setConnectionPoint(ConnectionPointBuilder.getConnectionPoint(processProvide));
                }
            }
        }

        return casa;
    }
}
