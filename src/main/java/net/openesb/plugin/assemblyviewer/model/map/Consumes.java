package net.openesb.plugin.assemblyviewer.model.map;

import org.w3c.dom.Element;

/**
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Consumes {
    
    private final com.sun.jbi.management.descriptor.Consumes consumes;
    
    public Consumes(com.sun.jbi.management.descriptor.Consumes consumes) {
        this.consumes = consumes;
    }
    
    public com.sun.jbi.management.descriptor.Consumes getTarget() {
        return this.consumes;
    }
    
    public String getDisplayName() {
        return getExtendedInformation("display-name");
    }
    
    public String getProcessName() {
        return getExtendedInformation("process-name");
    }
    
    private String getExtendedInformation(String elementName) {
        if (this.consumes != null && this.consumes.getAnyOrAny() != null) {
            for(Element elt : this.consumes.getAnyOrAny()) {
                if (elt.getLocalName().equals(elementName)) {
                    return elt.getTextContent();
                }
            }
        }
        return "";
    }
}
