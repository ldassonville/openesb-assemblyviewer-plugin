package net.openesb.plugin.assemblyviewer.model.map;

import org.w3c.dom.Element;

/**
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Provides {
    
    private final com.sun.jbi.management.descriptor.Provides provides;
    
    public Provides(com.sun.jbi.management.descriptor.Provides provides) {
        this.provides = provides;
    }
    
    public com.sun.jbi.management.descriptor.Provides getTarget() {
        return this.provides;
    }
    
    public String getDisplayName() {
        return getExtendedInformation("display-name");
    }
    
    public String getProcessName() {
        return getExtendedInformation("process-name");
    }
    
    private String getExtendedInformation(String elementName) {
        if (this.provides != null && this.provides.getAnyOrAny() != null) {
            for(Element elt : this.provides.getAnyOrAny()) {
                if (elt.getLocalName().equals(elementName)) {
                    return elt.getTextContent();
                }
            }
        }
        
        //What ? empty ...pojo ?
        if(provides.getEndpointName() != null){
        	return provides.getEndpointName();
        }
        return "";
    }
}
