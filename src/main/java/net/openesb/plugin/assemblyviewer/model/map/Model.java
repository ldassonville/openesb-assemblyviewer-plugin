package net.openesb.plugin.assemblyviewer.model.map;

import java.util.ArrayList;
import java.util.List;

import net.openesb.plugin.assemblyviewer.model.map.resolver.Link;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public class Model {

    private List<Connector> connectors = new ArrayList<Connector>();
    private List<Assembly> assemblies = new ArrayList<Assembly>();
    private List<Link> links = new ArrayList<Link>();

    public List<Connector> getConnectors() {
        return connectors;
    }

    public void setConnectors(List<Connector> connectors) {
        this.connectors = connectors;
    }

    public List<Assembly> getAssemblies() {
        return assemblies;
    }

    public void setAssemblies(List<Assembly> assemblies) {
        this.assemblies = assemblies;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
