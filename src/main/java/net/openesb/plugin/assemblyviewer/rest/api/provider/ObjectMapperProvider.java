package net.openesb.plugin.assemblyviewer.rest.api.provider;

import java.io.IOException;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.namespace.QName;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

/**
 * TODO javadoc.
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
public class ObjectMapperProvider implements ContextResolver<ObjectMapper> {

    private final ObjectMapper defaultObjectMapper;

    public ObjectMapperProvider() {
        defaultObjectMapper = createDefaultMapper();
    }

    @Override
    public ObjectMapper getContext(Class<?> type) {
        return defaultObjectMapper;
    }

    private static ObjectMapper createDefaultMapper() {
        AnnotationIntrospector jaxbIntrospector = new JaxbAnnotationIntrospector();
        ObjectMapper mapper = new ObjectMapper();

        mapper.configure(Feature.INDENT_OUTPUT, true);

        // Using JAXB de-serializer
        mapper.setDeserializationConfig(mapper.getDeserializationConfig().withAnnotationIntrospector(jaxbIntrospector));
        mapper.setSerializationConfig(mapper.getSerializationConfig().withAnnotationIntrospector(jaxbIntrospector));

        SimpleModule customModule = new SimpleModule("OpenesbModule", new Version(1, 0, 0, null));
        customModule.addSerializer(new QNameSerializer());
        mapper.registerModule(customModule);
        
        return mapper;
    }
    
    public static class QNameSerializer extends JsonSerializer<QName> {

        @Override
        public void serialize(QName value, JsonGenerator jgen, SerializerProvider provider)
                throws IOException, JsonProcessingException
        {
            jgen.writeStartObject();
            
            jgen.writeStringField("namespaceURI", value.getNamespaceURI());
            jgen.writeStringField("localPart", value.getLocalPart());
            
            jgen.writeEndObject();
        }

        @Override
        public Class<QName> handledType() {
            return QName.class;
        }
    }
}