package net.openesb.plugin.assemblyviewer.rest.api.provider;

import net.openesb.plugin.assemblyviewer.service.MapService;
import net.openesb.plugin.assemblyviewer.service.MapServiceImpl;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public class ServiceInjectionProvider extends AbstractBinder {
	
    @Override
    protected void configure() {
        bind(new MapServiceImpl()).to(MapService.class);
    }
    
}
