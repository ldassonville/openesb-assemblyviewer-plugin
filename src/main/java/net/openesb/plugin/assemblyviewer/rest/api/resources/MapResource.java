package net.openesb.plugin.assemblyviewer.rest.api.resources;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.openesb.management.api.ManagementException;
import net.openesb.plugin.assemblyviewer.model.PluginInfo;
import net.openesb.plugin.assemblyviewer.model.map.Model;
import net.openesb.plugin.assemblyviewer.plugin.AssemblyViewerPlugin;
import net.openesb.plugin.assemblyviewer.service.MapService;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
@Path("/")
public class MapResource extends AbstractResource {

	private static final Logger LOG = Logger.getLogger(MapResource.class.getPackage().getName());
	
	@Context
 	private MapService serviceAssemblyService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public PluginInfo getInformations() {
        return AssemblyViewerPlugin.getInfos();
    }
    
    /**
     * Provide assembly JSON model 
     * @param assemblyName 
     * @return JSON model
     * @throws ManagementException
     */
    @GET
    @Path("map/{assemblyName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Model getMapModel(@PathParam("assemblyName") String assemblyName) throws ManagementException {
      
    	LOG.info("Requesting map for assembly : "+assemblyName);
    	return serviceAssemblyService.getMapModel(assemblyName);
    }
    
}
