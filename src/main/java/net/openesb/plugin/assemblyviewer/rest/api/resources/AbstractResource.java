package net.openesb.plugin.assemblyviewer.rest.api.resources;

import java.util.logging.Logger;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public abstract class AbstractResource {
    
    private static Logger sLogger = null;
    

    /**
     * Get the logger for the REST services.
     *
     * @return java.util.Logger is the logger
     */
    protected Logger getLogger() {
        // late binding.
        if (sLogger == null) {
            sLogger = Logger.getLogger("net.openesb.standalone.statistic.rest.api");
        }
        return sLogger;
    }
    


    public static String name(String name, String... names) {
        final StringBuilder builder = new StringBuilder();
        append(builder, name);
        if (names != null) {
            for (String s : names) {
                append(builder, s);
            }
        }
        return builder.toString();
    }
    
    private static void append(StringBuilder builder, String part) {
        if (part != null && !part.isEmpty()) {
            if (builder.length() > 0) {
                builder.append('.');
            }
            builder.append(part);
        }
    }
}
