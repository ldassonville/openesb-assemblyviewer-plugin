package net.openesb.plugin.assemblyviewer.rest.api;

import net.openesb.plugin.assemblyviewer.rest.api.provider.ObjectMapperProvider;
import net.openesb.plugin.assemblyviewer.rest.api.provider.ServiceInjectionProvider;
import net.openesb.plugin.assemblyviewer.rest.api.resources.MapResource;
import net.openesb.rest.api.provider.CrossDomainFilter;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public class AssemblyViewerApplication extends ResourceConfig {

	public AssemblyViewerApplication() {
		super(
				// Register Root Resources
				MapResource.class,

				// Register Jackson ObjectMapper resolver
				ObjectMapperProvider.class, 
                
                // Register filter
                // BasicAuthenticationFilter.class,
                CrossDomainFilter.class,
                
				//Jersey features
				JacksonFeature.class);

		register(new ServiceInjectionProvider());
	}
}
