package net.openesb.plugin.assemblyviewer.module;

import net.openesb.plugin.assemblyviewer.service.MapService;
import net.openesb.plugin.assemblyviewer.service.MapServiceImpl;
import net.openesb.plugin.assemblyviewer.spi.AssemblyViewerService;

import com.google.inject.AbstractModule;

/**
 *
 * @author ldassonville
 */
public class MapModule extends AbstractModule {

    @Override
    protected void configure() {
    	
        bind(AssemblyViewerService.class).asEagerSingleton();

        bind(MapService.class).to(MapServiceImpl.class);
    }
}
