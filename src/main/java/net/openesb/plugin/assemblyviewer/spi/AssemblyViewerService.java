package net.openesb.plugin.assemblyviewer.spi;

import java.util.logging.Logger;

import net.openesb.plugin.assemblyviewer.rest.api.AssemblyViewerApplication;
import net.openesb.standalone.Lifecycle;
import net.openesb.standalone.LifecycleException;
import net.openesb.standalone.http.HttpServer;

import com.google.inject.Inject;

/**
 * 
 * @author ldassonville
 * 
 */
public class AssemblyViewerService implements Lifecycle {

	private static final Logger LOG = Logger.getLogger(AssemblyViewerService.class.getPackage().getName());
	
	@Inject
	private HttpServer httpServer;

	@Override
	public void start() throws LifecycleException {

		LOG.info("AssemblyViewer starting");
		
		registerRestAPI();

		LOG.info("AssemblyViewer started");
	}

	@Override
	public void stop() throws LifecycleException{
		LOG.info("AssemblyViewer stopped");
	}

	/**
	 * Register rest API.
	 */
	private void registerRestAPI(){
		LOG.info("Registering AssemblyViewer rest api");	
		httpServer.addRestHandler(new AssemblyViewerApplication(), "/assemblyviewer");	
	}
}
