package net.openesb.plugin.assemblyviewer.service;

import net.openesb.management.api.ManagementException;
import net.openesb.plugin.assemblyviewer.model.map.Model;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public interface MapService {
        
	/**
	 * Return the meta-model of service assembly
	 * 
	 * @param assemblyName
	 * @return
	 * @throws ManagementException
	 */
    Model getMapModel(String assemblyName) throws ManagementException;
}
